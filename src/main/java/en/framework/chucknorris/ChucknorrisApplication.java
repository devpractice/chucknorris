package en.framework.chucknorris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"classpath:chuck-config.xml"}) // plus adaugare facet in IntelliJ
public class ChucknorrisApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChucknorrisApplication.class, args);
    }
}
