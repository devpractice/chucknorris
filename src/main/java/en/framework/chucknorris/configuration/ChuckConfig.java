package en.framework.chucknorris.configuration;

import guru.springframework.norris.chuck.ChuckNorrisQuotes;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChuckConfig {

    //@Bean -- este configurat prin XML
    public ChuckNorrisQuotes chuckNorrisQuotes() {
        return new ChuckNorrisQuotes();
    }

}
