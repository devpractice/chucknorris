package en.framework.chucknorris.controller;

import en.framework.chucknorris.service.JokeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class JokesController {

    private static final Logger log = LoggerFactory.getLogger(JokesController.class);
    private final JokeService jokeService;

    @Autowired
    public JokesController(JokeService jokeService) {
        this.jokeService = jokeService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getJokes(Model model) {
        model.addAttribute("chuckJokes", jokeService.getJokes());
        model.addAttribute("chuckJokes2", jokeService.getJokes());
        log.info("Sending Message to pagie view service");
        return "index";
    }
}
